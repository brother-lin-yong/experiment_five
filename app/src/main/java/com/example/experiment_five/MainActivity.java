package com.example.experiment_five;

import androidx.appcompat.app.AppCompatActivity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static int MODE=MODE_PRIVATE;
    public static final String PREFERENCE_NAME="SaveSetting";
    Button btnLogin;
    EditText etAccount,etPassword;
    SharedPreferences sharedPreferences;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLogin=findViewById(R.id.btn_login);
        etAccount=findViewById(R.id.account_input);
        etPassword=findViewById(R.id.password_input);
        /**完成代码***/
        sharedPreferences = getSharedPreferences(PREFERENCE_NAME,MODE);
        btnLogin.setOnClickListener(this);
        String account = sharedPreferences.getString("account",null);
        String password = sharedPreferences.getString("password", null);
        if(account != null && password != null){
            etAccount.setText(account);
            etPassword.setText(password);
        }
    }

    @Override public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_login:
                /**完成代码***/
                String account=etAccount.getText().toString();
                String password=etPassword.getText().toString();
                Toast.makeText(this,"账号和密码已保存",Toast.LENGTH_LONG).show();
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("account",account);
                editor.putString("password",password);
                editor.commit();
                break; }

    }
}